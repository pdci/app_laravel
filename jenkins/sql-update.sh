
#!/bin/bash -ex
echo "${SVN_URL}" > tmp.sh
#sed -i "s/origin\///g" tmp.sh
#rm -rf script_out
#mkdir script_out
rm -rf log_sql
mkdir log_sql
NOME=$(cat tmp.sh)
echo ${NOME}

username_db_jenkins=usr_jenkins
password_db_jenkins=usr_jenkins
dbname=db_dev_cotec
username=usr_lafsisbio
password=usr_lafsisbio
servidor=dbdsv01

    vartest=$(PGPASSWORD=$password_db_jenkins psql -d $dbname -U $username_db_jenkins  -h $servidor -p 5432 -t -c "SELECT count(*) FROM information_schema.tables  WHERE table_schema='jenkins' and table_name = 'controle_versao';")

    if [ $vartest = 0 ]; then
     PGPASSWORD=$password psql -d $dbname -U $username  -h $servidor -p 5432 -t -c  "CREATE TABLE jenkins.controle_versao (des_tag text NOT NULL,  script_rodado text NOT NULL,data_inclusao text NOT NULL DEFAULT now(),ordem serial NOT NULL,CONSTRAINT _controle_versao_pk PRIMARY KEY (des_tag, script_rodado)) WITH (OIDS=FALSE);"
    # PGPASSWORD=$password psql -d $dbname -U $username  -h $servidor -p 5432 -t -c  "ALTER TABLE jenkins.controle_versao OWNER TO postgres;"
     PGPASSWORD=$password psql -d $dbname -U $username  -h $servidor -p 5432 -t -c  "COMMENT ON TABLE jenkins.controle_versao IS 'controle das atualizações via script realizada pela tag no ambiente e qual script de banco foi rodado';"

	 PGPASSWORD=$password psql -d $dbname -U $username  -h $servidor -p 5432 -t -c "CREATE INDEX controle_versao_pk_i ON jenkins.controle_versao USING btree (des_tag COLLATE pg_catalog.\"default\", script_rodado COLLATE pg_catalog.\"default\");"
     PGPASSWORD=$password psql -d $dbname -U $username  -h $servidor -p 5432 -t -c "ALTER ROLE usr_jenkins SET search_path TO jenkins;"
	fi

for entry in sql/*
do

  if [ -f "$entry" ]; then

    vartest=$(PGPASSWORD=$password_db_jenkins psql -d $dbname -U $username_db_jenkins  -h $servidor -p 5432 -t -c "SELECT count(*) FROM jenkins.controle_versao WHERE script_rodado = '$entry';")

    if [ $vartest = 0 ]; then

        echo "Rodando script :: $entry"

        PGPASSWORD=$password psql -d $dbname -U $username -h  $servidor -p 5432 -L log_$entry.log  -a < $entry &> log_$entry.out.log

        RODADOSQL=$(cat log_$entry.out.log)

        flag=`echo $RODADOSQL|awk '{print match($0,"ERROR:")}'`

        if [ $flag = 0 ]; then

            PGPASSWORD=$password  psql -d $dbname -U $username -h $servidor -p 5432 -t -c "INSERT INTO jenkins.controle_versao(des_tag, script_rodado)VALUES ('${NOME}', '$entry');" &> log_$entry.ins.out.log

            REGISTRAATUALIZACAO=$(cat log_$entry.ins.out.log)

            flag2=`echo $REGISTRAATUALIZACAO|awk '{print match($0,"ERROR:")}'`

            if [ $flag2 = 0 ]; then

             echo "#------------------------------------#"
             echo "SQL registrado no controle de versao"
             echo "#------------------------------------#"

            else

              echo "#----------------------------------------#"
              echo "#Erro ao digitar no controle de versao"
              echo "#----------------------------------------#"

              echo $REGISTRAATUALIZACAO
              exit 1

            fi
        else
            echo "#------------------------------------#"
            echo "FAILLLLLLLLLLLLL"
            echo "#------------------------------------#"
            exit 1
        fi

         # REGISTRAATUALIZACAO=$(psql -d $dbname -U $username -h $servidor -p 5432 -t -c "INSERT INTO jenkins.controle_versao(des_tag, script_rodado)VALUES ('${NOME}', '$entry');")


    else
      echo "#----------------------------------------------------------------------------------------------------------------------#"
      echo "#Script $entry já tinha sido rodado no banco ${_COMPONENTES_CONEXAO_BANCO_} do servidor ${_URL_DOMINIO_HOMOLOGACAO_}   #"
      echo "#----------------------------------------------------------------------------------------------------------------------#"
     # echo "diferente de 0"
    fi
  else
	echo "DIRETORIO"
  fi
#echo $vartest
done