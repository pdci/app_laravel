#!/usr/bin/env bash

PDCI_BD_IP=192.168.56.133
PDCI_BD_PORT=55432
PDCI_BD_NAME=db_dev_cotec
PDCI_REGISTRY_BD_DES=${JOB_REGISTRY}/${JOB_GIT_REPOSITORIO}:db-DSV
PDCI_ARQUIVO_SCHEMAS=$(pwd)"/conf/db.schemas"
PDCI_ARQUIVO_DUMP_CUSTOMIZADO=$(pwd)"/db_dev_cotec-customizado.sql"

#docker rmi $(docker images -f "dangling=true" -q)
ID_IMAGE=$(docker images --filter=reference='${JOB_REGISTRY}/${JOB_GIT_REPOSITORIO}:db-DSV' | grep db-DSV | awk '{print $3}')
if [ -z "${docker}" ];then
	docker pull registry.gitlab.com/pdci/pdci_db_dump:PRD
    docker tag  registry.gitlab.com/pdci/pdci_db_dump:PRD ${JOB_REGISTRY}/${JOB_GIT_REPOSITORIO}:db-DSV
fi

docker images
docker ps

ID=$(docker ps -a | grep ${JOB_REGISTRY}/${JOB_GIT_REPOSITORIO}:db-DSV | awk '{print $1}')
echo "ID => ${ID}"

if [ -z "${ID}" ];then
  echo "Subindo container da imagem base dump ${JOB_REGISTRY}/${JOB_GIT_REPOSITORIO}:db-DSV"
  docker container prune -f
  #docker pull ${JOB_REGISTRY}/${JOB_GIT_REPOSITORIO}:db-DSV
  docker run --name ${JOB_NAME}-dsv -p ${PDCI_BD_PORT}:5432 -d ${JOB_REGISTRY}/${JOB_GIT_REPOSITORIO}:db-DSV
  ID=$(docker ps -a | grep ${JOB_REGISTRY}/${JOB_GIT_REPOSITORIO}:db-DSV | awk '{print $1}')
fi

#esperar conexao com o banco de dados
psql_on=$(psql -U postgres -w -h ${PDCI_BD_IP} -p ${PDCI_BD_PORT}  -l 2>/dev/null | grep "List of databases")
count=0
while [ -z "${psql_on}" ]; do
  if [ ${count} -lt 60 ]; then
    echo "Esperando conexao com o banco em   ${count}/15 segundos"
    let count=count+1
    sleep 1
    psql_on=$(psql -U postgres -w -h ${PDCI_BD_IP} -p ${PDCI_BD_PORT}  -l 2>/dev/null | grep "List of databases")
    if [ -z "${psql_on}" ]; then
     echo "..."
    else
     echo ""
     echo "Conexao estabelecida com o banco de dados."
     echo ""
    fi
  else
    psql_on="ESGOTOU O TEMPO DE ESPERA"
    echo "Tempo esgotado de esperar pela conexao ao banco de dados"
  fi
done
#FIM #esperar conexao com o banco de dados


pwd
ls -lah

if [ -e "${PDCI_ARQUIVO_SCHEMAS}" ] ; then
	echo
	echo "Existe arquivo de customizacao de banco no projeto. ${PDCI_ARQUIVO_SCHEMAS}"
    echo
	if [ -e "${PDCI_ARQUIVO_DUMP_CUSTOMIZADO}" ] ; then
      echo
      echo "Arquivo de dump customizado ja existe"
      echo "Pulando para fase de restauracao"
      echo

    else
      echo "o arquivo ${PDCI_ARQUIVO_SCHEMAS} existe"
      schemas=$(cat conf/db.schemas)
      echo
      echo "relacao de esquemas informados pelo programador, que o  sistemas utiliza:  informados pelo programador que o sistemas utiliza ${schemas}"
      echo

#--dependent_ns.nspname as schema_utiliza
#--, dependent_view.relname as dependent_view
# source_ns.nspname as app_consome_schema
#--, source_table.relname as source_table
#--, pg_attribute.attname as column_name
#--, pg_attribute.*
#--, pg_depend.*
      comando_sql_include="select string_agg(''''||tb5.app_consome_schema||'''', ', ') as app_consome_schema from (SELECT distinct source_ns.nspname as app_consome_schema FROM pg_depend JOIN pg_rewrite ON pg_depend.objid = pg_rewrite.oid JOIN pg_class as dependent_view ON pg_rewrite.ev_class = dependent_view.oid JOIN pg_class as source_table ON pg_depend.refobjid = source_table.oid JOIN pg_attribute ON pg_depend.refobjid = pg_attribute.attrelid     AND pg_depend.refobjsubid = pg_attribute.attnum JOIN pg_namespace dependent_ns ON dependent_ns.oid = dependent_view.relnamespace JOIN pg_namespace source_ns ON source_ns.oid = source_table.relnamespace WHERE dependent_ns.nspname in (select schema_name from information_schema.schemata where schema_name in ('pg_toast','pg_temp_1','pg_toast_temp_1','pg_catalog','public','information_schema','tiger','tiger_data','topology',${schemas})) UNION select distinct schema_name as app_consome_schema from information_schema.schemata where schema_name in ('pg_toast','pg_temp_1','pg_toast_temp_1','pg_catalog','public','information_schema','tiger','tiger_data','topology', ${schemas})) as tb5;"
      echo
      echo "comando_sql_include=> ${comando_sql_include}"
      include_schema=$(psql -U postgres -w -h ${JOB_DOCKER_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -t -c "${comando_sql_include}")
      echo
      echo "Relacao de esquemas dependentes ou obrigatorios, informados pelo jenkins, atravês do processo de descobertas de dependencia do pdci = ${include_schema}"
      echo
      comando_sql_exclude="select string_agg('--exclude-schema='||schema_name, ' ') as app_consome_schema from information_schema.schemata where schema_name not in (${include_schema});"
      echo
      echo "comando_sql_exclude=> ${comando_sql_exclude}"

      exclude_schema=$(psql -U postgres -w -h ${JOB_DOCKER_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -t -c "${comando_sql_exclude}")
      echo
      echo "exclude_schema = ${exclude_schema}"
      echo


      exclude_dump=$(pg_dump -U postgres -w -h ${JOB_DOCKER_HOST} -p ${PDCI_BD_PORT}    ${exclude_schema}  --disable-triggers ${PDCI_BD_NAME}  -f ${PDCI_ARQUIVO_DUMP_CUSTOMIZADO} )
      echo
      echo "exclude_dump = ${exclude_dump}"
      echo
    fi

    if [ -e "${PDCI_ARQUIVO_DUMP_CUSTOMIZADO}" ] ; then
      echo
      echo "Restaurando banco customizado :: ${PDCI_ARQUIVO_DUMP_CUSTOMIZADO}"
      echo
      psql -U postgres -w -h ${JOB_DOCKER_HOST} -p ${PDCI_BD_PORT} -c "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = '${PDCI_BD_NAME}'";
      psql -U postgres -w -h ${JOB_DOCKER_HOST} -p ${PDCI_BD_PORT} -c "DROP DATABASE IF EXISTS ${PDCI_BD_NAME}" ;
      psql -U postgres -w -h ${JOB_DOCKER_HOST} -p ${PDCI_BD_PORT} -c "CREATE DATABASE ${PDCI_BD_NAME}  WITH OWNER = postgres; ";
      psql -U postgres -w -h ${JOB_DOCKER_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "create extension IF NOT EXISTS fuzzystrmatch;";
      psql -U postgres -w -h ${JOB_DOCKER_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "create extension IF NOT EXISTS postgis;";
      psql -U postgres -w -h ${JOB_DOCKER_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "create extension IF NOT EXISTS postgis_tiger_geocoder;";
      psql -U postgres -w -h ${JOB_DOCKER_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "create extension IF NOT EXISTS postgis_topology;";
      psql -U postgres -w -h ${JOB_DOCKER_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "create extension IF NOT EXISTS pg_trgm;";
      psql -U postgres -w -h ${JOB_DOCKER_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "create extension IF NOT EXISTS tablefunc;";
      psql -U postgres -w -h ${JOB_DOCKER_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "create extension IF NOT EXISTS postgis;";
      psql -U postgres -w -h ${JOB_DOCKER_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "create extension IF NOT EXISTS postgis_tiger_geocoder;";
      psql -U postgres -w -h ${JOB_DOCKER_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "create extension IF NOT EXISTS postgis_topology;";
      psql -U postgres -w -h ${JOB_DOCKER_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "create extension IF NOT EXISTS pg_trgm;";
      psql -U postgres -w -h ${JOB_DOCKER_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} -c "create extension IF NOT EXISTS tablefunc;";

      psql -U postgres -w -h ${JOB_DOCKER_HOST} -p ${PDCI_BD_PORT} -d ${PDCI_BD_NAME} < ${PDCI_ARQUIVO_DUMP_CUSTOMIZADO}

     # psql -U postgres -w -h ${JOB_DOCKER_HOST} -p 66432 -l
      echo "Excluir arquivo de dump customizado para liberar espaço"
      rm -rf ${PDCI_ARQUIVO_DUMP_CUSTOMIZADO}
    fi
    echo
    echo "Restauração customizada finalizada"
    echo
else
	echo
	echo "o arquivo ${PDCI_ARQUIVO_SCHEMAS} não existe. Sem personalizacao de banco"
    echo
fi