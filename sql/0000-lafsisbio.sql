﻿--
-- TOC entry 18 (class 2615 OID 18879)
-- Name: lafsisbio; Type: SCHEMA; Schema: -; Owner: usr_lafsisbio
--

-- CREATE SCHEMA if not exists lafsisbio;

--
-- TOC entry 1433 (class 1259 OID 122978)
-- Name: migrations; Type: TABLE; Schema: lafsisbio; Owner: usr_lafsisbio
--
BEGIN;
--TRUNCATE jenkins.controle_versao;
--DROP TABLE lafsisbio.migrations cascade;
--DROP TABLE lafsisbio.todos cascade;
--DROP TABLE lafsisbio.users cascade;
--DROP TABLE lafsisbio.password_resets cascade;


CREATE TABLE lafsisbio.migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);

GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE  lafsisbio.migrations TO usr_lafsisbio;

--
-- TOC entry 1432 (class 1259 OID 122976)
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: lafsisbio; Owner: usr_lafsisbio
--

CREATE SEQUENCE lafsisbio.migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

GRANT USAGE,SELECT ON SEQUENCE lafsisbio.migrations_id_seq TO usr_lafsisbio;

--
-- TOC entry 8368 (class 0 OID 0)
-- Dependencies: 1432
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: lafsisbio; Owner: usr_lafsisbio
--

ALTER SEQUENCE lafsisbio.migrations_id_seq OWNED BY lafsisbio.migrations.id;


--
-- TOC entry 1436 (class 1259 OID 122997)
-- Name: password_resets; Type: TABLE; Schema: lafsisbio; Owner: usr_lafsisbio
--

CREATE TABLE lafsisbio.password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);


GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE  lafsisbio.password_resets TO usr_lafsisbio;

--
-- TOC entry 1438 (class 1259 OID 123006)
-- Name: todos; Type: TABLE; Schema: lafsisbio; Owner: usr_lafsisbio
--

CREATE TABLE lafsisbio.todos (
    id integer NOT NULL,
    text text NOT NULL,
    finished boolean DEFAULT false NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE  lafsisbio.todos TO usr_lafsisbio;

--
-- TOC entry 1437 (class 1259 OID 123004)
-- Name: todos_id_seq; Type: SEQUENCE; Schema: lafsisbio; Owner: usr_lafsisbio
--

CREATE SEQUENCE lafsisbio.todos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


GRANT USAGE,SELECT ON SEQUENCE lafsisbio.todos_id_seq TO usr_lafsisbio;


--
-- TOC entry 8369 (class 0 OID 0)
-- Dependencies: 1437
-- Name: todos_id_seq; Type: SEQUENCE OWNED BY; Schema: lafsisbio; Owner: usr_lafsisbio
--

ALTER SEQUENCE lafsisbio.todos_id_seq OWNED BY lafsisbio.todos.id;


--
-- TOC entry 1435 (class 1259 OID 122986)
-- Name: users; Type: TABLE; Schema: lafsisbio; Owner: usr_lafsisbio
--

CREATE TABLE lafsisbio.users (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE  lafsisbio.users TO usr_lafsisbio;

--
-- TOC entry 1434 (class 1259 OID 122984)
-- Name: users_id_seq; Type: SEQUENCE; Schema: lafsisbio; Owner: usr_lafsisbio
--

CREATE SEQUENCE lafsisbio.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

GRANT USAGE,SELECT ON SEQUENCE lafsisbio.users_id_seq TO usr_lafsisbio;


--
-- TOC entry 8370 (class 0 OID 0)
-- Dependencies: 1434
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: lafsisbio; Owner: usr_lafsisbio
--

ALTER SEQUENCE lafsisbio.users_id_seq OWNED BY lafsisbio.users.id;


--
-- TOC entry 8050 (class 2604 OID 122981)
-- Name: migrations id; Type: DEFAULT; Schema: lafsisbio; Owner: usr_lafsisbio
--

ALTER TABLE ONLY lafsisbio.migrations ALTER COLUMN id SET DEFAULT nextval('lafsisbio.migrations_id_seq'::regclass);


--
-- TOC entry 8052 (class 2604 OID 123009)
-- Name: todos id; Type: DEFAULT; Schema: lafsisbio; Owner: usr_lafsisbio
--

ALTER TABLE ONLY lafsisbio.todos ALTER COLUMN id SET DEFAULT nextval('lafsisbio.todos_id_seq'::regclass);


--
-- TOC entry 8051 (class 2604 OID 122989)
-- Name: users id; Type: DEFAULT; Schema: lafsisbio; Owner: usr_lafsisbio
--

ALTER TABLE ONLY lafsisbio.users ALTER COLUMN id SET DEFAULT nextval('lafsisbio.users_id_seq'::regclass);


--
-- TOC entry 8371 (class 0 OID 0)
-- Dependencies: 1432
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: lafsisbio; Owner: usr_lafsisbio
--

SELECT pg_catalog.setval('lafsisbio.migrations_id_seq', 3, true);


--
-- TOC entry 8372 (class 0 OID 0)
-- Dependencies: 1437
-- Name: todos_id_seq; Type: SEQUENCE SET; Schema: lafsisbio; Owner: usr_lafsisbio
--

SELECT pg_catalog.setval('lafsisbio.todos_id_seq', 2, true);


--
-- TOC entry 8373 (class 0 OID 0)
-- Dependencies: 1434
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: lafsisbio; Owner: usr_lafsisbio
--

SELECT pg_catalog.setval('lafsisbio.users_id_seq', 1, false);


--
-- TOC entry 8055 (class 2606 OID 122983)
-- Name: migrations migrations_pkey; Type: CONSTRAINT; Schema: lafsisbio; Owner: usr_lafsisbio
--

ALTER TABLE ONLY lafsisbio.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- TOC entry 8062 (class 2606 OID 123015)
-- Name: todos todos_pkey; Type: CONSTRAINT; Schema: lafsisbio; Owner: usr_lafsisbio
--

ALTER TABLE ONLY lafsisbio.todos
    ADD CONSTRAINT todos_pkey PRIMARY KEY (id);


--
-- TOC entry 8057 (class 2606 OID 122996)
-- Name: users users_email_unique; Type: CONSTRAINT; Schema: lafsisbio; Owner: usr_lafsisbio
--

ALTER TABLE ONLY lafsisbio.users
    ADD CONSTRAINT users_email_unique UNIQUE (email);


--
-- TOC entry 8059 (class 2606 OID 122994)
-- Name: users users_pkey; Type: CONSTRAINT; Schema: lafsisbio; Owner: usr_lafsisbio
--

ALTER TABLE ONLY lafsisbio.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- TOC entry 8060 (class 1259 OID 123003)
-- Name: password_resets_email_index; Type: INDEX; Schema: lafsisbio; Owner: usr_lafsisbio
--

CREATE INDEX password_resets_email_index ON lafsisbio.password_resets USING btree (email);


-- Completed on 2018-11-14 19:34:31 -02

--
-- PostgreSQL database dump complete
--

END;