#!/usr/bin/env bash

#wget https://gitlab.com/pdci/image_file_dump/raw/master/docker-compose-customizar-db.yml?inline=false
#mv docker-compose-customizar-db.yml?inline=false docker-compose-customizar-db.yml
#docker container prune -f
#docker system prune -f
#docker-compose -f docker-compose-customizar-db.yml up
#docker tag registry.gitlab.com/pdci/image_file_dump/image_file_dump-prd:latest registry.gitlab.com/pdci/app_laravel/image_file_dump-dev:latest
#docker push registry.gitlab.com/pdci/app_laravel/image_file_dump-dev:latest


docker run --name db_base_full_latest -p 55432:5432    -v $(pwd)/postgres:/var/lib/postgresql/10/main  -e POSTGRES_DB=gis -e POSTGRES_USER=docker -e POSTGRES_PASS=docker -e ALLOW_IP_RANGE=0.0.0.0/0 -d  registry.gitlab.com/pdci/postgis:latest

docker run -v $(pwd)/conf:/conf registry.gitlab.com/pdci/image_file_dump/image_file_dump-prd:latest pdci_restaurar_db.sh

#ID=$(docker ps -a | grep registry.gitlab.com/pdci/postgis | grep latest | awk '{print $1}')
#echo "ID => ${ID}"
#cmd="docker commit ${ID}  registry.gitlab.com/pdci/postgis-PRD:latest"
#exec_cmd=$(${cmd})
#docker commit $ID  registry.gitlab.com/pdci/postgis-PRD:latest
#docker push registry.gitlab.com/pdci/postgis-PRD:latest