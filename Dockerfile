FROM registry.gitlab.com/pdci/co7_httpd_laravel:production  AS production
#LABEL Vendor="COTEC-LAF-SISBio"
#LABEL Description="Centos 7 , PHP 7.2, composer  "
#LABEL License=GPLv2
#LABEL Version=0.0.1

ENV APP_HOME /var/www/html

WORKDIR $APP_HOME

RUN rm -rf $APP_HOME/*

COPY conf/etc/httpd/conf/httpd.conf  /etc/httpd/conf/httpd.conf
COPY conf/laravel.conf /etc/httpd/conf.d/laravel.conf

COPY src/  $APP_HOME

#COPY conf/etc/php.ini /etc/php.ini
#COPY conf/etc/httpd/conf/httpd.conf /etc/httpd/conf/httpd.conf
COPY src/.env.app.example $APP_HOME/.env

RUN composer clearcache && \
    composer install --no-dev --optimize-autoloader

EXPOSE 443 80 8000 5001

RUN chown apache:apache $APP_HOME -R
RUN chmod 760 $APP_HOME -R

#ADD run-httpd.sh /run-httpd.sh
#RUN chmod -v +x /run-httpd.sh

CMD ["/run-httpd.sh"]

FROM registry.gitlab.com/pdci/co7_httpd_laravel:testing AS testing

COPY conf/etc/httpd/conf/httpd.conf  /etc/httpd/conf/httpd.conf
COPY conf/laravel.conf /etc/httpd/conf.d/laravel.conf

ENV APP_HOME /var/www/html

ENV PDCI_SONARQUBE_URL_PORT=${PDCI_SONARQUBE_URL_PORT:-http://localhost:9000}
# Instalacao do node para os testes

WORKDIR $APP_HOME

RUN rm -rf $APP_HOME/*

COPY src/  $APP_HOME

COPY src/.env.app.dev $APP_HOME/.env

WORKDIR $APP_HOME

VOLUME $APP_HOME

RUN composer clearcache && \
  composer install && \
  yarn install

RUN chown apache:apache $APP_HOME -R
RUN chmod 760 $APP_HOME -R

CMD ["/run-httpd.sh"]